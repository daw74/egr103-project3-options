# egr103-project3-options

If you still do not have satisfactory images for the challenge step of Project 3,
and you do not want to wait for your specific images to be available, this
repository provides two sets of images that you could use to address the challenge
step.  Develop some detailed comparisons between the two images of your choice,
develop good plots to show the differences, and describe/analyze what they mean.

## Forest Loss
As described at <ttps://www.washingtonpost.com/climate-environment/2022/05/12/ghost-forests-carolina-climate-change/>,
North Carolina's coastal forests are losing trees and marshland due to sea level rise and storms.

Two images are provided from the Aligator River National Wildlife Refuge from 2009 and 2022 in which
you can estimate forest loss.


## Fall Foliage
The fall foliage display in New England is famous for its bright colors.  How much do things really
change from summer to fall?  Two images are provided on the change from August to November.

